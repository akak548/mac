function bash_reload {
	if [ -f ~/.bash_profile ]; then
		source ~/.bash_profile
	elif [ -f ~/.bashrc ]; then
		source ~/.bashrc
  fi
}

function env_update {
	pushd $HOME/Projects/akak548/mac
	bash setup.sh update
	# Install VIM Plugins
	vim +PluginInstall +qall

	# Install Tmux Plugins
	~/.tmux/plugins/tpm/bin/install_plugins

	# Compile YouCompleteMe
	python3 ~/.vim/bundle/YouCompleteMe/install.py --all
	popd
}

