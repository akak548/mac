# Testing
PATH="/usr/local/opt/coreutils/libexec/gnubin:$PATH"
MANPATH="/usr/local/opt/coreutils/libexec/gnuman:$MANPATH"

DIR_COLORS=`which dircolors`
test -e ~/.bash/dircolors && eval `$DIR_COLORS -b ~/.bash/dircolors`

alias ls="ls -alh --color=always" 
alias grep="grep --color=always"
alias egrep="egrep --color=always"

