#!/bin/bash

declare -a menu=()

# A very hacky associative array (hash)
p_dir=("/Users/cgreene/Goworkspaces/src" "/Users/cgreene/Projects")
max_depth=("3" "2")
min_depth=("3" "2")
cut_depth=("6" "5")

cur=0

generate_menu() {
	case $1 in 
		g|go) _index=0;;
		p|project) _index=1;;
	esac

	project=$(find ${p_dir[$_index]} -type directory -maxdepth ${max_depth[$_index]} -mindepth ${min_depth[$_index]} | sort)
	for p in ${project}
	do
		result=$(echo $p | cut -d/ -f ${cut_depth[$_index]}-)
		menu=( "${menu[@]}" "$result")
	done
}

draw_menu() {
    for i in "${menu[@]}"; do
        if [[ ${menu[$cur]} == $i ]]; then
            tput setaf 2; echo " > $i"; tput sgr0
        else
            echo "   $i";
        fi
    done
}

clear_menu()  {
    for i in "${menu[@]}"; do tput cuu1; done
    tput ed
}


function project() {
	generate_menu $1
	draw_menu 
	while read -sn1 key; do # 1 char (not delimiter), silent
    # Check for enter/space
    if [[ "$key" == "" ]]; then break; fi

    # catch multi-char special key sequences
    #read -sn1 -t .0001 k1 2> /dev; read -sn1 -t 0.0001 k2; read -sn1 -t 0.0001 k3 2> /dev/null
    key+=${k1}${k2}${k3}
    read -sn1 -t .5 k1 2> /dev/null
    key+=${k1}

    case "$key" in
        # cursor up, left: previous item
        i|j|$'\e[A'|$'\e0A'|$'\e[D'|$'\e0D') ((cur > 0)) && ((cur--));;
        # cursor down, right: next item
        k|l|$'\e[B'|$'\e0B'|$'\e[C'|$'\e0C') ((cur < ${#menu[@]}-1)) && ((cur++));;
        # home: first item
        $'\e[1~'|$'\e0H'|$'\e[H')  cur=0;;
        # end: last item
        $'\e[4~'|$'\e0F'|$'\e[F') ((cur=${#menu[@]}-1));;
         # q, carriage return: quit
        q|''|$'\e')echo "Aborted." && exit;;
    esac
    # Redraw menu
    clear_menu
    draw_menu
	done

	_select_dir="${p_dir[$_index]}/${menu[$cur]}"
	cd $(echo $_select_dir | tr -d '\r')

	unset menu
}
