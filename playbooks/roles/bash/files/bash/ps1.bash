## PS1

# Bash Colors
Blue="\[\e[0;34m\]"
LBlue="\[\e[1;34m\]"
Green="\[\e[0;32m\]"
LGreen="\[\e[1;32m\]"
Cyan="\[\e[0;36m\]"
LCyan="\[\e[1;36m\]"
Red="\[\e[0;31m\]"
LRed="\[\e[1;31m\]"
Purple="\[\e[0;35m\]"
LPurple="\[\e[1;35m\]"
Brown="\[\e[0;33\]"
Yellow="\[\e[1;33m\]"
LGray="\[\e[0;37m\]"
White="\[\e[1;37m\]"
Orange="\[\e[1;96m\]"
CReset="\[\e[30;1m\]"

function user {
	if [ $UID == 0 ]; then
		echo "${Red}root${CReset}"
	else
		echo "${White}\u${CReset}"
	fi
 	
}


function platform {
	case $PLATFORM_ID in
		'baremetal')
			echo "${Blue}${PLATFORM_ID}${CReset}"
			;;
		'docker')
			echo "${Cryan}${PLATFORM_ID}${CReset}"
			;;
		*)
			echo "${LBlue}${PLATFORM_ID}${Creset}"
			;;
	esac
}

function ps1_git { 
	git_branch=`git branch --no-color 2> /dev/null | sed -e '/^[^*]/d' | awk '{print $2}'`
	git_status=`git status 2> /dev/null`
   
  if [[ $git_branch =~ 'master' ]]; then
    git_color=$Red
  else
    git_color=$LGray
  fi 

	if [[ $git_status =~ "Your branch is ahead of" ]]; then
		git_icon='^'
  elif [[ $git_status =~ "nothing to commit" ]]; then
		git_icon='-'
	elif [[ ! $git_status =~ "working directory clean" ]]; then
		git_icon='+'
  else
	  git_icon='>'
  fi

  echo "git-(${git_color}${git_branch} ${git_icon}${CReset})$ "
}

function ps1_default {
 
	case $PLATFORM_ID in
		'baremetal')
			host_color=$Blue
			;;
		'docker')
			host_color=$Cryan
			;;
		*)
			host_color=$LBlue
			;;
	esac
	host="$host_color\h${CReset}"
	PS1_TIME="$LPurple\t \d$CReset"
	#public_ip=`dig +short myip.opendns.com @resolver1.opendns.com`
	private_ip=`ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1' | head -n 1`
  
	echo "\n${CReset}($(user)@$host)-($private_ip)-($PS1_TIME)-->${CReset}\n"
}

function ps1_dir {
	_pwd="$Green\w$CReset"
	dir_info="${LPurple}\$(ls -1 | wc -l | sed 's: ::g') files, \$(ls -lah | grep -m 1 total | sed 's/total //')b ${CReset}"
	echo "dir-($_pwd)-($dir_info)-->\n"
}

function ps1_cli {
  echo "${Orange}cli${CReset}- $ "
}

function _prompt {
	_PS1=''
	
	_PS1+=$(ps1_default)$(ps1_dir)

  git status > /dev/null 2>&1
  if [ $? != '128' ]; then
		_PS1+=$(ps1_git)
  else
    _PS1+=$(ps1_cli)
  fi

  _PS1+="\[\e[0m\]"
	export PS1=$_PS1
}

PROMPT_COMMAND=_prompt

