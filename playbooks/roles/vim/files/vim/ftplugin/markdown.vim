autocmd BufNewFile,BufRead *.md set wrapmargin=0
autocmd BufNewFile,BufRead *.md set cc=0
autocmd vimenter *.md NerdTree 
autocmd VimEnter * wincmd p
