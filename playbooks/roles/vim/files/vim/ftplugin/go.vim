" Replace tab with spaces
"au BufNewFile,BufRead *.go setlocal tabstop=4
au Filetype go set tabstop=4 et sw=4

" ========== Appearances ============="
au BufEnter *.go colorscheme lucariox

" Automatically open/close Nerdtree
autocmd vimenter *.go NERDTree
autocmd VimEnter * wincmd p
let g:go_fmt_autosave = 0
let g:go_code_completion_enabled = 1

let g:go_highlight_array_whitespace_error = 1
let g:go_highlight_chan_whitespace_error = 1
let g:go_highlight_trailing_whitespace_error = 1
let g:go_highlight_operators = 1
let g:go_highlight_functions = 0

" ALE Plugin" 
let g:ale_enabled = 1
let g:ale_change_sign_column_color = 1
let g:ale_completion_enabled = 0
let g:ale_echo_cursor = 1
let b:ale_fixers = ['gofmt', 'golint']
let g:ale_completion_enabled = 1

