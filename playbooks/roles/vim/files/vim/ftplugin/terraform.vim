" ========== Appearances ============="
 au BufEnter *.tf colorscheme sublimemonokai

set expandtab tabstop=2 shiftwidth=2

" Automatically open/close Nerdtree
autocmd vimenter *.tf NERDTree
autocmd VimEnter * wincmd p
autocmd StdinReadPre * let s:std_in=2

" Terraform Plugin "
let g:terraform_align = 0
let g:terraform_fold_sections = 1
let g:terraform_fmt_on_save = 1

" ALE Plugin" 
let g:ale_enabled = 1
let g:ale_change_sign_column_color = 1
let g:ale_completion_enabled = 1
let g:ale_echo_cursor =  1
let g:UltiSnipsSnippetDirectories = ["UltiSnips", "~/.vim/snippets", "~/.vim/bundle/vim-terraform-snippets"]
