nnoremap <silent> <F5> :!clear;python %<CR>

" Replace tab with spaces
au Filetype py setl et ts=4 sw=4

" ========== Appearances ============="
 au BufEnter *.py colorscheme sublimemonokai

" Automatically open/close Nerdtree
autocmd vimenter *.py NERDTree
autocmd VimEnter * wincmd p
autocmd StdinReadPre * let s:std_in=1

let g:pymode = 1
let g:pymode_python = 'python3'
let g:pymode_warnings = 1
let g:pymode_indent = 1
let g:pymode_options_colorcolumn = 1
let g:pymode_lint = 1
let g:pymode_lint_checkers = ['pep8', 'pylint']
let g:pymode_lint_message = 1
let g:pymode_virtualenv = 1
let g:pymode_motion = 1

let g:pymode_syntax = 1
let g:pymode_syntax_all = 1
let g:pymode_syntax_print_as_function = 1
let g:pymode_syntax_highlight_self = g:pymode_syntax_all
let g:pymode_syntax_indent_errors = g:pymode_syntax_all
let g:pymode_syntax_space_errors = g:pymode_syntax_all
let g:pymode_syntax_string_formatting = g:pymode_syntax_all
let g:pymode_syntax_string_format = g:pymode_syntax_all
let g:pymode_syntax_string_templates = g:pymode_syntax_all
let g:pymode_syntax_doctests = g:pymode_syntax_all
let g:pymode_syntax_builtin_objs = g:pymode_syntax_all
let g:pymode_syntax_builtin_types = g:pymode_syntax_all
let g:pymode_syntax_highlight_exceptions = g:pymode_syntax_all

let NERDTreeIgnore=['\.pyc$', '\~$']

" ALE Plugin" 
let g:ale_enabled = 1
let g:ale_change_sign_column_color = 1
let g:ale_completion_enabled = 1
let g:ale_echo_cursor = 1
