#PLAYBOOK_ID=`hostname | awk -F. '{print $1}' | awk -F- '{print $2}'`
echo $PLAYBOOK_ID

function clean {

	echo "WARNING : This will remove complete wipe the mac and all configurations"
	echo -n "are you sure you want to do that? [y/n] : "
	read confirmation

	if [ $confirmation == "y" ]; then
		/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/uninstall)"
		exit 0
	else
  		echo "keeping everything intact"
  		exit 0
	fi
}

function install {
	echo "==========================================="
	echo "Setting up your mac using akak548/mac"
	echo "==========================================="

	# Install Ansible
	sudo easy_install pip3
	sudo pip3 install ansible
	
	#installdir=$(mktemp -d -t macsetup.XXXXXX)

	# Clone Repository
	#git clone https://gitlab.com/akak548/mac.git $installdir 

	#if [ ! -d $installdir ]; then
    #		echo "failed to find setupmac."
   # 		echo "git cloned failed"
   # 		exit 1
#	else
#
#    		cd $installdir 
	ansible-playbook -i ./hosts --limit "dutchie" --verbose playbooks/playbook.yml
#	fi
#	
#	if [ ! -d ~/.ssh ]; then
#		echo "Setting up ssh"
#		ssh-keygen -b 4096 -t rsa -f ~/.ssh/id_rsa
#	else
#		echo ".ssh directory already exists"
#	fi	
#
#	echo "cleaning up..."
#	rm -Rfv /tmp/$installdir
#
#	echo "and we are done! Enjoy!"
}

function update {
    ansible-playbook -i ./hosts -vvv playbooks/playbook.yml
}

function check {
    ansible-playbook -i ./hosts -vvv --check playbooks/playbook.yml
}

function tmux_install {
	

	brew install automake pkg-config && autoconf -i
	# create tmp directry
	installdir=$(mktemp -d -t macsetup_tmux.XXXXXX)

	# Change directories
	pushd ${installdir}

	#Download zip file
	curl https://github.com/tmux/tmux/archive/2.8.zip -L -o 2.8.zip
	#Open File
	unzip 2.8.zip
	cd tmux-2.8
	#Install tmux
	sh autogen.sh && ./configure && make && make install

	#verify tmux exists
	popd
	#rm -Rf ${installdir}
}

function initialize {
	# Install brew
	./scripts/brew_install.sh

	brew install wget automake

	tmux_install
}

case $1 in
    update)
        update
        ;;
    clean)
	clean
	;;
    update-all)
        update
        update-submodule
        ;;
    tmux)
        tmux_install
	;;
    install)
      install
      ;;
    check)
      check
      ;;
esac

exit 0
