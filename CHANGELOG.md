hangelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.0] - 2019-09-13
### Added
- Installed WTF Dashboard and added basic dashboard for home
- Installed Browsh for terminal browsing
- Installed Firefox for Browsh
- Started Changelog.md file

### Configured
- None

### Removed
- None
